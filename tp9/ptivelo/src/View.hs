{-# LANGUAGE OverloadedStrings #-}

module View where

import Control.Monad
import Lucid
import qualified Data.Text as T
import Model

renderToFile :: FilePath -> Html a ->IO ()
renderToFile = Lucid.renderToFile

viewAlbum :: [Rider] -> Html ()
viewAlbum riders = 
    doctypehtml_ $ do
        head_ $ meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "Album"
            forM_ riders $ \r -> do
                h2_ $ toHtml $ name r
                div_ $ forM_ (images r) $ \i -> do
                    a_ [href_ (T.pack i)] $ do
                        img_ [src_ (T.pack i), height_ "100px"]