{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

module Model where

import GHC.Generics
import Data.Aeson

data Rider = Rider 
    { name :: String
    , images :: [String] 
    } deriving (Show, Generic)

instance ToJSON Rider