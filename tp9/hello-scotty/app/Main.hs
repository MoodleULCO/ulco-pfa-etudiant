{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import Data.Maybe (fromMaybe)
import Data.Aeson hiding (json)
import Network.Wai.Middleware.Gzip (gzip, def, gzipFiles, GzipFiles(..))
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import System.Environment (lookupEnv)
import Web.Scotty
import GHC.Generics
import Lucid

import qualified Data.Text.Lazy as L

data Person = Person
    { _name :: L.Text
    , _year :: Int
    } deriving Generic

instance ToJSON Person

person :: Person
person = Person "toto" 42

myPage :: Html ()
myPage = do
    doctype_ 
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "ma page"
        body_ $ do
             h1_ "this is"
             p_ "mypage"
             ul_ $ do
                li_ $ do
                    a_ [href_ "/html1"] "html1"


main :: IO ()
main = do
    port <- read . fromMaybe "3000" <$> lookupEnv "PORT"
    scotty port $ do
        middleware logStdoutDev
        middleware $ gzip def { gzipFiles = GzipCompress }
        middleware $ staticPolicy $ addBase "static"
        get "/" $ html $ renderText myPage
        get "/route1" $ text "ROUTE 1"
        get "/route1/route2" $ text "ROUTE 2"
        get "/html1" $ html "<h1>this is some html</h1>"
        get "/json1" $ json ("toto"::String, 42::Int)
        get "/json2" $ json person
        get "/add1:nb1/:nb2" $ do 
            nb1 <- param "nb1"
            nb2 <- param "nb2"
            json $ (nb1 + nb2::Int)

        get "/add2:nb1/:nb2" $ do 
            nb1 <- param "nb1" `rescue` (\_ -> return 0)
            nb2 <- param "nb2" `rescue` (\_ -> return 0)
            json $ (nb1 + nb2::Int)
        
        get "/index" $ redirect "/"
