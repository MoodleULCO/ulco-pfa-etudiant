
fizzbuzz1 :: [Int] -> [String]
fizzbuzz1 (x:xs) 
    | (x `mod` 15) == 0 = "fizzbuzz" : (fizzbuzz1 xs)
    | (x `mod` 3) == 0 = "fizz" : (fizzbuzz1 xs)
    | (x `mod` 5) == 0 = "buzz" : (fizzbuzz1 xs)
    | otherwise = (show x) : (fizzbuzz1 xs)

fizzbuzz2 :: [Int] -> [String]
fizzbuzz2 (x:xs) = fizz (x:xs) []
    where fizz [] acc = acc
          fizz (x:xs) acc
            | (x `mod` 15) == 0 = fizz xs (acc ++ ["fizzbuzz"])
            | (x `mod` 3) == 0 = fizz xs (acc ++ ["fizz"])
            | (x `mod` 5) == 0 = fizz xs (acc ++ ["buzz"])
            | otherwise = fizz xs (acc ++ [show x])
-- fizzbuzz
fizzbuzz = fizzbuzz1 [1..]

main :: IO ()
main = do
    print (fizzbuzz1 [1..15])

