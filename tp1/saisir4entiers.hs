import Control.Monad (forM_)
import System.IO (hFlush, stdout)
import Text.Read (readMaybe)

saisir :: IO()
saisir = do
    putStrLn $ "saisie : "
    nombre <- getLine
    case readMaybe nombre of
        Just x -> putStrLn ("vous avez saisi l'entier " ++ show (x :: Int))
        Nothing -> putStrLn "saisie incorrecte"


