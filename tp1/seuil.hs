-- seuilInt
seuilInt :: Int -> Int -> Int -> Int
seuilInt a b c
    | (a < b && a > c) = a
    | (b > a && b < c) = b
    | otherwise = c

-- seuilTuple
seuilTuple :: (Int, Int) -> Int -> Int
seuilTuple (a,b) c = seuilInt a b c

main :: IO ()
main = do
    print (seuilInt 1 10 0)
    print (seuilInt 1 10 2)
    print (seuilInt 1 10 42) 
    print (seuilTuple (1,10) 5)

