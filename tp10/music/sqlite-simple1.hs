{-# LANGUAGE OverloadedStrings #-}

import Data.Text
import Database.SQLite.Simple

selectAllTitles :: Connection -> IO [(Int,Int,Text)]
selectAllTitles conn = query_ conn "SELECT * FROM title"

main :: IO ()
main = withConnection "music.db" selectAllTitles >>= mapM_ print