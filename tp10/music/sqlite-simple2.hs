{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

import Data.Text (Text)
import Database.SQLite.Simple
import GHC.Generics (Generic)

data Artist = Artist 
    { artist_id   :: Int
    , artist_name :: Text
    } deriving (Generic, Show)

instance FromRow Artist where
    fromRow = Artist <$> field <*> field 

selectAll :: Connection -> IO [Artist]
selectAll conn = query_ conn 
    "SELECT * FROM artist"

main :: IO ()
main = do
    conn <- open "music.db"
    res1 <- selectAll conn
    mapM_ print res1
