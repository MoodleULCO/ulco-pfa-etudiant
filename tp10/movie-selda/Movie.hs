{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE OverloadedLabels #-}
{-# LANGUAGE TypeOperators #-}

module Movie where

import Control.Monad.IO.Class (liftIO)
import Database.Selda
import Database.Selda.SQLite

----------------------------------------------------------------------
-- Movie
----------------------------------------------------------------------

data Movie = Movie
  { movie_id :: ID Movie
  , movie_title :: Text
  , movie_year :: Int
  } deriving (Generic, Show)

instance SqlRow Movie

movie_table :: Table Movie
movie_table = table "movie" [#movie_id :- autoPrimary]

----------------------------------------------------------------------
-- Person
----------------------------------------------------------------------

data Person = Person
  { person_id :: ID Person
  , person_name :: Text
  } deriving (Generic, Show)

instance SqlRow Person

person_table :: Table Person
person_table = table "person" [#person_id :- autoPrimary]

----------------------------------------------------------------------
-- Role
----------------------------------------------------------------------

data Role = Role
  { role_id :: ID Role
  , role_name :: Text
  } deriving (Generic, Show)

instance SqlRow Role

role_table :: Table Role
role_table = table "role" [#role_id :- autoPrimary]


----------------------------------------------------------------------
-- Prod
----------------------------------------------------------------------

data Prod = Prod
  { prod_movie :: ID Movie
  , prod_person :: ID Person
  , prod_role :: ID Role
  } deriving (Generic, Show)

instance SqlRow Prod

prod_table :: Table Prod
prod_table = table "prod" [(#prod_movie :+ #prod_person :+ #prod_role) :- primary]

----------------------------------------------------------------------
-- queries
----------------------------------------------------------------------

dbInit :: SeldaT SQLite IO ()
dbInit = do

    createTable movie_table
    tryInsert movie_table
        [ Movie def "Bernie" 1996
        , Movie def "Le Kid" 1921 
        , Movie def "Metropolis" 1927
        , Movie def "Citizen Kane" 1941 ]
        >>= liftIO . print

    createTable person_table
    tryInsert person_table
        [ Person def "Orson Welles"
        , Person def "Charlie Chaplin" 
        , Person def "Albert Dupontel"
        , Person def "Claude Perron"
        , Person def "Alfred Abel"
        , Person def "Fritz Lang"]
        >>= liftIO . print

    createTable role_table
    tryInsert role_table
        [ Role def "Réalisateur"
        , Role def "Acteur" 
        , Role def "Producteur"]
        >>= liftIO . print


    createTable prod_table
    tryInsert prod_table
        [ Prod (toId 1) (toId 3) (toId 1)
        , Prod (toId 1) (toId 3) (toId 2)
        , Prod (toId 1) (toId 4) (toId 2)
        , Prod (toId 2) (toId 2) (toId 1)
        , Prod (toId 2) (toId 2) (toId 2)
        , Prod (toId 2) (toId 2) (toId 3)
        , Prod (toId 3) (toId 5) (toId 2)
        , Prod (toId 3) (toId 6) (toId 1) 
        , Prod (toId 4) (toId 1) (toId 1)
        , Prod (toId 4) (toId 1) (toId 2)
        , Prod (toId 4) (toId 1) (toId 3) ]
        >>= liftIO . print

----------------------------------------------------------------------
-- queries
----------------------------------------------------------------------
dbSelectAllMovies :: SeldaT SQLite IO [Movie]
dbSelectAllMovies = query $ select movie_table

{--dbSelectAllProds :: SeldaT SQLite IO [Text :*: Text]
dbSelectAllProds = query $ do
    movies <- select movie_table
    persons <- select person_table
    roles <- select role_table
    prods <- select prod_table

    restrict (prods ! #prod_movie .== movies ! #movie_id)
    restrict (prods ! #prod_person .== persons ! #person_id)
    restrict (prods ! #prod_role .== roles ! #role_id)

    return (artists ! #artist_name :*: titles ! #title_name)--}

dbSelectAllMoviesFromId :: Int -> SeldaT SQLite IO [Movie]
dbSelectAllMoviesFromId id_movie = query $ do
    movies <- select movie_table
    restrict (movies ! #movie_id .== literal (toId id_movie))
    return movies