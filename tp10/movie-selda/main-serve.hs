{-# LANGUAGE OverloadedStrings #-}

import Database.Selda.SQLite 
import Web.Scotty
import Control.Monad.IO.Class
import Control.Monad
import Lucid

import Movie

dbFilename :: String
dbFilename = "movie.db"

main :: IO ()
main = scotty 3000 $ do
    get "/" $ do
        movies <- liftIO(withSQLite "movie.db" Movie.dbSelectAllMovies) 
        liftIO(print movies)

        html $ renderText $ myPage movies

myPage :: [Movie] -> Html ()
myPage movies = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "mypage"
        body_ $ div_ [class_ "container"] $ do
            h1_ "movies"
            ul_ $ forM_ movies $ \m -> li_ $ do 
                toHtml $ movie_title m
                " ("
                toHtml $ show (movie_year m) 
                ")"