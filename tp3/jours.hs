data Jour = Lundi | Mardi | Mercredi | Jeudi | Vendredi | Samedi | Dimanche

estWeekend :: Jour -> Bool
estWeekend Samedi = True
estWeekend Dimanche = True
estWeekend _ = False

compterOuvrables :: [Jour] -> Int
compterOuvrables xs = aux xs 0
    where aux [] acc = acc
          aux (y:ys) acc
                | estWeekend y == False = aux ys (acc+1)
                | otherwise = aux ys acc
--compterOuvrables xs = length (filter (not . estWeekend) xs)

main :: IO ()
main = do
    print $ estWeekend Lundi
    print $ estWeekend Samedi

    print $ compterOuvrables [Lundi,Mardi,Samedi,Jeudi]

