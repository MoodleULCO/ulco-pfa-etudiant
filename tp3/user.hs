
-- TODO User
data User = User 
            { name :: String,
              fname :: String, 
              age :: Int}

showUser :: User -> String
showUser u = name u ++ " " ++ fname u ++ " " ++ (show (age u))

incAge :: User -> User
incAge u = u {age = age u + 1}

main :: IO ()
main = let 
    u = User "toto" "tata" 50
    in do 
        print $ showUser u
        print $ showUser $ incAge u
        
    

