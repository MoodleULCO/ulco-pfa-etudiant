
-- mymap1 
mymap1 :: (a -> b) -> [a] -> [b]
mymap1 _ [] = []
mymap1 f (x:xs) = f x : mymap1 f xs 

-- mymap2
mymap2 :: (a -> b) -> [a] -> [b]
mymap2 f xs = aux xs []
    where aux [] acc = acc
          aux (y:ys) acc = aux ys (acc ++ [f y])

-- myfilter1 
myfilter1 :: (a -> Bool) -> [a] -> [a]
myfilter1 _ [] = []
myfilter1 f (x:xs)
    | f x = x : (myfilter1 f xs)
    | otherwise = myfilter1 f xs

-- myfilter2 
myfilter2 :: (a -> Bool) -> [a] -> [a]
myfilter2 f xs = aux xs []
    where aux [] acc = acc
          aux (y:ys) acc = aux ys (if f y then acc ++ [y] else acc)

-- myfoldl 

-- myfoldr 

main :: IO ()
main = do
    print $ mymap1 (*2) [1..5::Int]
    print $ mymap2 (*2) [1..5::Int]

    print $ myfilter1 (>3) [1..5::Int]
    print $ myfilter2 (>3) [1..5::Int]
