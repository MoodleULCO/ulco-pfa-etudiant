import Data.Char

-- isSorted
isSorted :: (Ord a) => [a] -> Bool
isSorted [] = True
isSorted [x] = True
isSorted (x:y:xs) = x < y && isSorted (y:xs)

-- nocaseCmp 
nocaseCmp str1 str2 = (map toUpper str1) < (map toUpper str2)

main :: IO ()
main = do
    print $ isSorted "abc"
    print $ "tata" `nocaseCmp` "TOTO"

