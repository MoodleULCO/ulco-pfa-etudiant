
-- safeTailString 
safeTailString :: [Char] -> [Char]
safeTailString [] = ""
safeTailString (_:xs) = xs

-- safeHeadString 
safeHeadString :: [Char] -> Maybe Char
safeHeadString [] = Nothing
safeHeadString (x:_) = Just x

-- safeTail 
safeTail :: [a] -> [a]
safeTail [] = []
safeTail (_:xs) = xs

-- safeHead 
safeHead :: [a] -> Maybe a
safeHead [] = Nothing
safeHead (x:_) = Just x

main :: IO ()
main = do
    print $ safeTailString "foobar"
    print $ safeTailString ""

    print $ safeTail [1..4]

    print $ safeHead [1..4]
    print $ safeHead ""

