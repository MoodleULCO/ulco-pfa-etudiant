import Data.List

-- threshList
--threshList _ [] = []
--threshList n list = map (\x -> if x > n then n else x) list
threshList :: Ord a => a ->[a] -> [a]
threshList n = map (min n)

-- selectList
selectList :: Ord a => a ->[a] -> [a]
selectList n = filter (<n)

-- maxList
maxList :: Ord a => [a] -> a
maxList = foldl1' max

main :: IO ()
main = do
    print $ threshList 3 [1..5::Int]
    print $ selectList 'c' ['a'..'k']
    print $ maxList [42, 35, 59::Int]
    print $ maxList "barfoo"

