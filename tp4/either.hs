type MyNum = Either String Double

showMyNum :: MyNum -> String
showMyNum (Left str) = "error: " ++ str
showMyNum (Right n) = "result: " ++ show n

mySqrt :: Double -> MyNum
mySqrt x 
    | x > 0 = Right (sqrt x)
    | otherwise = Left "nombre négatif"

myLog :: Double -> MyNum
myLog x 
    | x > 0 = Right (log x)
    | otherwise = Left "nombre négatif"

myMul2 :: Double -> MyNum
myMul2 x = Right (2*x)

myNeg :: Double -> MyNum
myNeg x = Right (-x)

myCompute :: MyNum
myCompute = do 
    x <- mySqrt 16
    y <- myNeg x
    myMul2 y
--myCompute = mySqrt 16 >>= myNeg >>= myMul2

main :: IO ()
main = putStrLn $ showMyNum myCompute

