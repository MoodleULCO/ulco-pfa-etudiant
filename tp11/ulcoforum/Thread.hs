{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}

module Thread where

import Data.Text (Text)
import Database.SQLite.Simple
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)
import GHC.Generics (Generic)

----------------------------------------------------------------------
-- datas
----------------------------------------------------------------------

data Thread = Thread
    { thread_id    :: Int
    , thread_theme :: Int
    , thread_user :: Text
    , thread_message :: Text
    } deriving (Generic, Show)

data Theme = Theme
    { theme_id :: Int
    , theme_name :: Text
    } deriving (Generic, Show)


instance FromRow Thread where
    fromRow = Thread <$> field <*> field <*> field <*> field

instance FromRow Theme where
    fromRow = Theme <$> field <*> field

----------------------------------------------------------------------
-- queries
----------------------------------------------------------------------
dbSelectAllData :: Connection -> IO [(Text, Text, Text)]
dbSelectAllData conn = query_ conn
    "SELECT theme_name, thread_user, thread_message FROM thread \
    \INNER JOIN theme ON thread_theme = theme_id "

dbSelectAllTheme :: Connection -> IO [Theme]
dbSelectAllTheme conn = query_ conn
    "SELECT * FROM theme "

dbSelectThemeById :: Connection -> Int -> IO [Theme]
dbSelectThemeById conn idTheme = query conn
    "SELECT * FROM theme WHERE theme_id = (?)"
    (Only idTheme)

dbSelectAllDataByThemeId :: Connection -> Int -> IO [(Text, Text)]
dbSelectAllDataByThemeId conn idTheme = query conn
    "SELECT thread_user, thread_message FROM thread \
    \INNER JOIN theme ON thread_theme = theme_id \
    \WHERE theme_id = (?)"
    (Only idTheme)