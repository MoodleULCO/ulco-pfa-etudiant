-- create database:
-- sqlite3 thread.db < thread.sql

CREATE TABLE theme (
  theme_id INTEGER PRIMARY KEY AUTOINCREMENT,
  theme_name TEXT
);

CREATE TABLE thread (
  thread_id INTEGER PRIMARY KEY AUTOINCREMENT, 
  thread_theme INTEGER, 
  thread_user TEXT,
  thread_message TEXT,
  FOREIGN KEY(thread_theme) REFERENCES theme(theme_id)
);

INSERT INTO theme VALUES(1, 'Vacances 2020-2021');
INSERT INTO theme VALUES(2, 'Master Info nouveau programme');


INSERT INTO thread VALUES(1, 1, 'John Doe', 'Pas de vacances cette année');
INSERT INTO thread VALUES(2, 1, 'Toto', 'Youpie!');
INSERT INTO thread VALUES(3, 2, 'Toto', 'Tous les cours passent en Haskell');