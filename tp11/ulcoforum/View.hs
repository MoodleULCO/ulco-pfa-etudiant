{-# LANGUAGE OverloadedStrings #-}

module View where

import Data.Text 
import Control.Monad
import Lucid

import Thread

headerPage :: Html ()
headerPage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
            title_ "ulcoforum"
        body_ $ div_ [class_ "container"] $ do
            h1_ $ do
                a_ [href_ "/"] "ulcoforum"
            div_ [class_ "links"] $ do
                p_ $ do
                    a_ [href_ "/alldata"] "All data"
                p_ $ do
                    a_ [href_ "/alltheme"] "All themes"

homePage :: Html ()
homePage = do
    headerPage
    p_ "this is ulcoforum"

allDatas :: [(Text, Text, Text)] -> Html ()
allDatas datas = do
    headerPage
    p_ "All data :"
    ul_ $ forM_ datas $ \(x, y, z) -> li_ $ do 
        div_ [class_ "theme"] $ do
            toHtml $ x
            " :"
        div_ [class_ "thread"] $ do
            toHtml $ y
            ": "
            toHtml $ z

allThemes :: [Theme] -> Html ()
allThemes themes = do
    headerPage
    p_ "All themes :"
    ul_ $ forM_ themes $ \t -> li_ $ do 
        div_ [class_ "theme"] $ do
            let src = "/addThread/" ++ show(theme_id t)
            a_ [href_ (pack src)] $ do
               toHtml $ theme_name t

addThread :: [(Text, Text)] -> [Theme] -> Html ()
addThread thread theme = do
    headerPage
    div_ $ forM_ theme $ \t -> do 
        p_ $ do
            toHtml $ theme_name t
        ul_ $ forM_ thread $ \(x,y)-> do 
            li_ $ do
                toHtml $ x
                ": "
                toHtml $ y
    form_ [action_ "addThread"] $ do
        input_ [type_ "text", name_ "user", placeholder_ "user"]
        br_ []
        input_ [type_ "text", name_ "message", placeholder_ "message"]
        br_ []
        input_ [type_ "submit", value_ "Valider"]
        