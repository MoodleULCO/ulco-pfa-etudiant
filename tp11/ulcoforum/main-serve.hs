{-# LANGUAGE OverloadedStrings #-}

import Web.Scotty
import Control.Monad.IO.Class
import Lucid
import Database.SQLite.Simple (open, close)

import Thread
import View

dbFilename :: String
dbFilename = "thread.db"

main :: IO ()
main = scotty 3000 $ do
    get "/" $ do
        html $ renderText $ homePage

    get "/alldata" $ do
        conn <- liftIO(open dbFilename)

        datas <- liftIO(Thread.dbSelectAllData conn) 
        liftIO(mapM_ print datas)

        html $ renderText $ allDatas datas

        liftIO(close conn)

    get "/alltheme" $ do
        conn <- liftIO(open dbFilename)

        themes <- liftIO(Thread.dbSelectAllTheme conn) 
        liftIO(mapM_ print themes)

        html $ renderText $ allThemes themes

        liftIO(close conn)
    
    get "/addThread/:idTheme" $ do 
        conn <- liftIO(open dbFilename)

        idTheme <- param "idTheme"

        threads <- liftIO(Thread.dbSelectAllDataByThemeId conn idTheme) 
        liftIO(mapM_ print threads)

        theme <- liftIO(Thread.dbSelectThemeById conn idTheme) 
        liftIO(mapM_ print theme)

        html $ renderText $ addThread threads theme

        liftIO(close conn)