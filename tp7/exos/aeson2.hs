{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Show)

instance FromJSON Person where 
    parseJSON = withObject "Person" $ \v -> Person
        <$> v .: "firstname"
        <*> v .: "lastname"
        <*> v .: "birthyear"
        <*> v .: "speakenglish"

{-instance FromJSON Person where 
    parseJSON = withObject "Person" $ \v -> do
        f <- v .: "firstname"
        l <- v .: "lastname"
        b <- v .: "birthyear"
        s <- v .: "speakenglish"
        return $ Person f l b s
-}

instance ToJSON Person where 
    toJSON (Person firstname lastname birthyear speakenglish) =
        object ["firstname" .= firstname,
                "lastname" .= lastname,
                "birthyear" .= birthyear,
                "speakenglish" .= speakenglish]

main :: IO ()
main = do
    let res0 = Person "John" "Doe" "1978" False
    print res0
    print $ encode res0
    res1 <- eitherDecodeFileStrict' "aeson-test1.json"
    print $ (res1 :: Either String Person)
    res2 <- eitherDecodeFileStrict' "aeson-test2.json"
    print $ (res2 :: Either String [Person])
    res3 <- eitherDecodeFileStrict' "aeson-test3.json"
    print $ (res3 :: Either String [Person])

