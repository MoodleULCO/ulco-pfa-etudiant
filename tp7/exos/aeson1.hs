{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import qualified Data.Aeson as A
import GHC.Generics

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Generic, Show)

instance A.FromJSON Person 
instance A.ToJSON Person

main :: IO ()
main = do
    let res0 = Person "John" "Doe" "1978" False
    print res0
    print $ A.encode res0
    res1 <- A.eitherDecodeFileStrict' "aeson-test1.json"
    print $ (res1 :: Either String Person)
    res2 <- A.eitherDecodeFileStrict' "aeson-test2.json"
    print $ (res2 :: Either String [Person])
    res3 <- A.eitherDecodeFileStrict' "aeson-test3.json"
    print $ (res3 :: Either String [Person])

