{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import Data.Yaml 
import GHC.Generics

data Person = Person
    { firstname    :: T.Text
    , lastname     :: T.Text
    , birthyear    :: T.Text
    , speakenglish :: Bool
    } deriving (Generic, Show)

instance FromJSON Person 

main :: IO ()
main = do
    res1 <- decodeFileEither "yaml-test1.yaml"
    print (res1 :: Either ParseException Person)

