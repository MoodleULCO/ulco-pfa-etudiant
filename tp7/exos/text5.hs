import qualified Data.Text.IO as T
import qualified Data.Text.Lazy as L
import qualified Data.Text.Lazy.IO as L

main :: IO ()
main = do
    file <- T.readFile "text5.hs"
    let str = L.fromStrict file
    L.putStrLn str 