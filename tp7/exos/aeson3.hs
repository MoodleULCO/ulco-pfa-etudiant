{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import Data.Aeson

data Person = Person
    { first    :: T.Text
    , last     :: T.Text
    , birth    :: Int
    } deriving (Show)

instance FromJSON Person where 
    parseJSON = withObject "Person" $ \v -> do
        f <- v .: "firstname"
        l <- v .: "lastname"
        b <- v .: "birthyear"
        return $ Person f l (read b)

main :: IO ()
main = do
    let res0 = Person "John" "Doe" 1970
    print res0
    res1 <- eitherDecodeFileStrict' "aeson-test1.json"
    print $ (res1 :: Either String Person)
    res2 <- eitherDecodeFileStrict' "aeson-test2.json"
    print $ (res2 :: Either String [Person])
    res3 <- eitherDecodeFileStrict' "aeson-test3.json"
    print $ (res3 :: Either String [Person])

