import qualified Data.Text.IO as T
import qualified Data.Text as T

main :: IO ()
main = do
    file <- T.readFile "text2.hs"
    let str = T.unpack file
    putStrLn str 