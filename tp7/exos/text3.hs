import qualified Data.ByteString.Char8 as BS
import qualified Data.Text.Encoding as T
import qualified Data.Text.IO as T

main :: IO ()
main = do
    file <- BS.readFile "text3.hs"
    let str = T.decodeUtf8 file
    T.putStrLn str 