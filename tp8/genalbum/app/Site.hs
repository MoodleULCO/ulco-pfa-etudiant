{-# LANGUAGE OverloadedStrings #-}

module Site where 

import Data.Aeson

data Site = Site
    { images :: [String]
    , url :: String
    } deriving (Show)

instance FromJSON Site where 
    parseJSON = withObject "Site" $ \v -> do
        f <- v .: "imgs"
        l <- v .: "url"
        return $ Site f l

loadSites :: FilePath -> IO (Maybe [Site])
loadSites = decodeFileStrict'

