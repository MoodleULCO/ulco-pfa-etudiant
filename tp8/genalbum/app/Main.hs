import Site
import ViewAlbum

main :: IO ()
main = do
    mSites <- loadSites "data/genalbum.json"
    case mSites of
        Nothing -> putStrLn "cannot read file"
        Just sites -> renderToFile "index.html" $ viewAlbum sites

