{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import GHC.Generics
import Data.Aeson
import qualified Data.Yaml as Y

data Address = Address
    { road        :: T.Text
      , zipcode   :: Int
      , city      :: T.Text
      , number    :: Int
    } deriving (Generic, Show)

data Person = Person
    { lastname    :: T.Text
    , adress      :: Address
    , birthyear   :: Int
    , firstname   :: T.Text
    } deriving (Generic, Show)

instance ToJSON Address
instance ToJSON Person

persons :: [Person]
persons =
    [ Person "Doe" (Address "Pont Vieux" 43000 "Espaly" 42) 1970 "John"
    , Person "Curry" (Address "Pere Lachaise" 75000 "Paris" 1337) 1900 "Haskell" 
    ]


main :: IO()
main = do
    encodeFile "yaml3.json" (persons)
    Y.encodeFile "yaml3.yaml" (persons)
