{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Aeson as A
import Data.HashMap.Strict
import Data.ByteString.Lazy.Char8 as BS

main :: IO()
main = do
    BS.putStrLn $ A.encode $ A.String "John"
    BS.putStrLn $ A.encode $ A.Number 21
    BS.putStrLn $ A.encode $ A.Object $ fromList [("birthyear", (A.Number 1995))]
    BS.putStrLn $ A.encode $ A.Object $ fromList [("lastname", (A.String "Doe")), ("birthyear", (A.Number 1995)), ("firstname", (A.String "John"))]
