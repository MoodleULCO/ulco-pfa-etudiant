{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}

import Lucid.Html5
import Lucid.Base

page :: Html ()
page = do 
    doctypehtml_ $ do 
        head_ $ do 
            meta_ [charset_ "utf_8"]
        body_ $ do
            h1_ "Hello" 
            img_ [src_ "toto.png"]
            p_ $ do 
                "this is "
                a_ [href_ "toto.png"] "a link"

main :: IO()
main = do
    renderToFile "maPage.html" page