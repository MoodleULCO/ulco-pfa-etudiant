{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ExtendedDefaultRules #-}

import Lucid.Html5
import Lucid.Base

page :: Html ()
page = h1_ "Hello" <> p_ "world"

main :: IO()
main = do
    print $ page