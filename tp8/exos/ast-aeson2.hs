{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Yaml as Y
import Data.HashMap.Strict
import Data.ByteString.Char8 as BS

main :: IO()
main = do
    BS.putStrLn $ Y.encode $ Y.String "John"
    BS.putStrLn $ Y.encode $ Y.Number 21
    BS.putStrLn $ Y.encode $ Y.Object $ fromList [("birthyear", (Y.Number 1995))]
    BS.putStrLn $ Y.encode $ Y.Object $ fromList [("lastname", (Y.String "Doe")), ("birthyear", (Y.Number 1995)), ("firstname", (Y.String "John"))]
