{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}

import qualified Data.Text as T
import qualified Data.Aeson as A
import GHC.Generics

data Adress = Adress
    { road        :: T.Text
      , zipcode   :: Int
      , city      :: T.Text
      , number    :: Int
    } deriving (Generic, Show)

data Person = Person
    { lastname    :: T.Text
    , adress      :: Adress
    , birthyear   :: Int
    , firstname   :: T.Text
    } deriving (Generic, Show)

instance A.ToJSON Adress
instance A.ToJSON Person

persons :: [Person]
persons =
    [ Person "Doe" (Adress "Pont Vieux" 43000 "Espaly" 42) 1970 "John"
    , Person "Curry" (Adress "Pere Lachaise" 75000 "Paris" 1337) 1900 "Haskell" 
    ]


main :: IO()
main = do
    A.encodeFile "aeson3.json" (persons)
